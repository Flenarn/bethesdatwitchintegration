//SKSE
#include "../skse/PluginAPI.h"
#include "../skse/skse_version.h"
#include "../skse/PapyrusVM.h"
#include "../skse/PapyrusNativeFunctions.h"

#include <../skse/skse/PapyrusNativeFunctions.cpp>

// Global
#include <Windows.h>
#include <shlobj.h>
#include <iostream>
#include <fstream>
#include <thread>

//Internal
#include "DirectoryChange.h"
#include "CommandHandler.h"
#include "TwitchIntegration.h"

static PluginHandle					g_pluginHandle = kPluginHandle_Invalid;
static SKSEPapyrusInterface			*g_Papyrus = NULL;

void startCleanup() {
	CHAR my_documents[MAX_PATH];
	HRESULT result = SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, SHGFP_TYPE_CURRENT, my_documents);

	char twitchCommandDump[MAX_PATH];
	sprintf_s(twitchCommandDump, "%s\\My Games\\Skyrim\\SKSE\\TwitchIntegration\\", my_documents);

	SetCurrentDirectory(twitchCommandDump);

	if (remove("command.txt") != 0) {
		//_MESSAGE("ERROR: Error deleting file.");
	}
	;
	if (remove("commandFeedback.txt") != 0) {
		//_MESSAGE("ERROR: Error deleting file.");
	}
}

void initScanThread() {
	startCleanup();
	std::thread TwitchThread(monitorDirectory);
	TwitchThread.detach();
}

class VMClassRegistry;

DWORD WINAPI InitializeConsole(LPVOID lpParam)
{
	// Maximum time to wait in milliseconds before giving up.
	// Usually console should be initialized within 20 seconds even on the slowest of computers.
	const int MaxWaitTime = 60000;

	int isGuiInitialized = 0;
	int waited = 0;
	while (waited < MaxWaitTime)
	{
		// All these are needed to exist.
		int guiManager = *((int*)0x12E35E4);
		int guiStringHolder = *((int*)0x12E35E0);
		int plrChar = *((int*)0x1B2E8E4);

		if (guiManager != 0 && guiStringHolder != 0 && plrChar != 0)
			isGuiInitialized++;

		// Wait a little bit because having a pointer does not mean it's fully initialized yet.
		if (isGuiInitialized >= 5)
		{
			// Toggle console twice to initialize it.
			const int toggleConsole = 0x847210;

			// Second one must be close because it happens on another thread and toggle twice fast would open twice
			// since console didn't have time to open yet.
			const int closeConsole = 0x847130;
			_asm
			{
				pushad
					pushfd
					call toggleConsole
					call closeConsole
					popfd
					popad
			}

			// Don't need this thread anymore.
			return 0;
		}

		// Wait until trying again.
		Sleep(50);
		waited += 50;
	}

	// Write error.
	_MESSAGE("Console initialize timed out.");
	return 0;
}

extern String CommandString;

namespace Injected_Functions {
	void TwitchWriteCommandReturn(StaticFunctionTag* base) {
		CHAR my_documents[MAX_PATH];
		HRESULT result = SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, SHGFP_TYPE_CURRENT, my_documents);

		char twitchCommandDump[MAX_PATH];

		sprintf_s(twitchCommandDump, "%s\\My Games\\Skyrim\\SKSE\\TwitchIntegration\\commandFeedback.txt", my_documents);

		const char* path = twitchCommandDump;

		std::ofstream file(path);
		return;
	}

	static BSFixedString TwitchPluginDependency(StaticFunctionTag* base) {
		BSFixedString result = CommandString.esmToUse.c_str();
		return result;
	}
}

bool Register_Functions(VMClassRegistry* registry) {
	registry->RegisterFunction(
		new NativeFunction0 <StaticFunctionTag, void>("TwitchWriteCommandReturn", "TwitchWriteCommandReturn", Injected_Functions::TwitchWriteCommandReturn, registry));
	registry->RegisterFunction(
		new NativeFunction0 <StaticFunctionTag, BSFixedString>("TwitchPluginDependency", "TwitchPluginDependency", Injected_Functions::TwitchPluginDependency, registry));
	return true;
}

extern "C"
{

	bool SKSEPlugin_Query(const SKSEInterface* skse, PluginInfo* info)
	{
		gLog.OpenRelative(CSIDL_MYDOCUMENTS, "\\My Games\\Skyrim\\SKSE\\skse_twitch_integration.log");
		gLog.SetPrintLevel(IDebugLog::kLevel_Error);
		gLog.SetLogLevel(IDebugLog::kLevel_DebugMessage);

		// populate info structure
		info->infoVersion = PluginInfo::kInfoVersion;
		info->name = "Twitch Integration Plugin";
		info->version = 1;

		g_pluginHandle = skse->GetPluginHandle();

		if (skse->isEditor || skse->runtimeVersion != RUNTIME_VERSION_1_9_32_0){
			return false;
		}

		return true;
	}

	bool SKSEPlugin_Load(const SKSEInterface * skse)
	{

		g_Papyrus = (SKSEPapyrusInterface*)skse->QueryInterface(kInterface_Papyrus);

		if (!g_Papyrus) {
			return false;
		} else if (!g_Papyrus->Register(Register_Functions)) {
			_MESSAGE("Twitch Integration functions registered!");
		}

		// Console must be initialized before we can use it. Open it and close immediately, however
		// UI must be initialized first so run another thread that waits until UI is initialized.
		DWORD threadId = 0;
		if (CreateThread(NULL, 0, InitializeConsole, NULL, 0, &threadId) == 0) {
			_MESSAGE("Failed to start console initialize thread!");
		}

		initScanThread();

		return true;
	}
};


