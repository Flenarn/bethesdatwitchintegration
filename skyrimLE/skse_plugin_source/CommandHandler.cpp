// Global
#include "string.h"
#include <iostream>
#include <sstream>
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <fstream>
#include <string>
#include <shlobj.h>
#include <cstdint>

// Internal
#include "Console.h"
#include "TwitchIntegration.h"

String CommandString;

void commandHandler(const char* str) {
	std::string command(str), identifierSpawn("spawn"), identifierItem("player.additem"), identifierWeather("weather");

	bool spawnCommand = (command.find(identifierSpawn) != std::string::npos);
	bool giveCommand = (command.find(identifierItem) != std::string::npos);
	bool weatherCommand(command.find(identifierWeather) != std::string::npos);

	//_MESSAGE(str);

	if (spawnCommand) {
		// Separate the command string into substrings for separate command usage.
		std::string array[5];
		std::stringstream ssin(str);

		int i = 0;
		while (ssin.good() && i < 5) {
			ssin >> array[i];
			i++;
		}

		std::string formId = array[1];
		std::string amount = array[2];
		std::string allowedInInterior = array[3];
		CommandString.esmToUse = array[4];

		// Convert the hexidecimal formID to a decimal that can be stored in the global.
		int formID_Decimal = std::stoi(formId, 0, 16);

		std::string formIdCommand = "set Twitch_FormID to " + std::to_string(formID_Decimal);
		std::string amountCommand = "set Twitch_SpawnAmount to " + amount;
		std::string interiorCommand = "set Twitch_InteriorAllowed to " + allowedInInterior;
		

		// Exectute the command structure
		ConsoleFunctions::ExecuteCommand_Internal(formIdCommand.c_str());
		ConsoleFunctions::ExecuteCommand_Internal(amountCommand.c_str());
		ConsoleFunctions::ExecuteCommand_Internal(interiorCommand.c_str());
		ConsoleFunctions::ExecuteCommand_Internal("setstage TwitchIntegrationQuest 10");
	} else if (giveCommand) {
		// Separate the command string into substrings for separate command usage.
		std::string array[4];
		std::stringstream ssin(str);

		int i = 0;
		while (ssin.good() && i < 4) {
			ssin >> array[i];
			i++;
		}

		std::string formId = array[1];
		std::string amount = array[2];
		CommandString.esmToUse = array[3];

		// Convert the hexidecimal formID to a decimal that can be stored in the global.
		int formID_Decimal = std::stoi(formId, 0, 16);

		std::string formIdCommand = "set Twitch_FormID to " + std::to_string(formID_Decimal);
		std::string amountCommand = "set Twitch_SpawnAmount to " + amount;

		// Exectute the command structure
		ConsoleFunctions::ExecuteCommand_Internal(formIdCommand.c_str());
		ConsoleFunctions::ExecuteCommand_Internal(amountCommand.c_str());
		ConsoleFunctions::ExecuteCommand_Internal("setstage TwitchIntegrationQuest 15");
	}
	else if (weatherCommand){
		// Separate the command string into substrings for separate command usage.
		std::string array[3];
		std::stringstream ssin(str);

		int i = 0;
		while (ssin.good() && i < 3) {
			ssin >> array[i];
			i++;
		}

		std::string formId = array[1];
		CommandString.esmToUse = array[2];

		// Convert the hexidecimal formId to a decimal that can be stored in the global.
		int formID_Decimal = std::stoi(formId, 0, 16);

		std::string formIdCommand = "set Twitch_FormID to " + std::to_string(formID_Decimal);

		// Execute the command structure
		ConsoleFunctions::ExecuteCommand_Internal(formIdCommand.c_str());
		ConsoleFunctions::ExecuteCommand_Internal("setstage TwitchIntegrationQuest 20");
	} else {
		ConsoleFunctions::ExecuteCommand_Internal(str);
	}
}