Scriptname Twitch extends Quest  

import TwitchWriteCommandReturn
import TwitchPluginDependency

;Don't touch this block of properties, doing so will break everything.
GlobalVariable Property Twitch_FormID auto
GlobalVariable Property Twitch_SpawnAmount auto
GlobalVariable Property Twitch_InteriorAllowed auto
Actor Property PlayerRef auto


;--CORE FUNCTIONS---------------------------------------------------------------------------------
Function SpawnNPC()
    ObjectReference PlayerRefObj = Game.GetPlayer()

    if(Twitch_InteriorAllowed.GetValueInt() == 0) ; 0 = not allowed, 1 = allowed
        if(PlayerRefObj.IsInInterior())
            Debug.Notification("Spawn failed, NPC spawn not allowed in interior cells.")
            TwitchWritecommandReturn()
            return
        endIf    
    endIf    

    ActorBase thisNPC

    ;Lets the streamer know something has spawned
	if(Twitch_SpawnAmount.GetValueInt() == 1) 
		Debug.Notification("Someone just spawned something...")
	Else
		Debug.Notification("Someone just spawned some things...")
	endIf

    thisNPC = Game.GetFormFromFile(Twitch_FormID.GetValueInt(), TwitchPluginDependency()) as ActorBase
  
    ;Creates a random distance from the player that the NPC(s) will spawn in.
    float RandomDistance = Utility.RandomFloat(-1024, 1024)

    int spawnedAmount = 0

    if(thisNPC != None)
        While (spawnedAmount < Twitch_SpawnAmount.GetValueInt())
            spawnedAmount = spawnedAmount +1
            ObjectReference kspawnedNPC = PlayerRefObj.PlaceAtMe(thisNPC, 1, false, true)
            Float fPlayerAngle = PlayerRef.GetAngleZ()
            Float fX = Math.sin(fPlayerAngle) * RandomDistance
            float fY = Math.cos(fPlayerAngle) * RandomDistance
            kspawnedNPC.MoveTo(PlayerRef, fx, fy)
            kspawnedNPC.Enable()
            ;if(kspawnedNPC as Actor) Look into solution to dance around this if possible. 
                ;kspawnedNPC.MoveToNearestNavmeshLocation()
            ;endIf    
        EndWhile
    endIf
EndFunction  

Function GiveItem()
	PlayerRef.AddItem(Game.GetFormFromFile(Twitch_FormID.GetValueInt(), TwitchPluginDependency()), Twitch_SpawnAmount.GetValueInt())
EndFunction	  

Function SetWeather()
    Weather thisWeather = Game.GetFormFromFile(Twitch_FormID.GetValueInt(), TwitchPluginDependency()) as Weather
    thisWeather.ForceActive()
    Debug.Notification("Someone just swapped the weather!")
EndFunction

;--USER DEFINED FUNCTIONS-------------------------------------------------------------------------
Function SlowPlayer()
    Debug.Notification("Your movement speed has been slowed down!")
    PlayerRef.SetActorValue("SpeedMult", 50)
    Utility.Wait(30)
    PlayerRef.SetActorValue("Speedmult", 100)
    Debug.Notification("Your movement speed is back to normal!")
EndFunction

Function Unequip()
	PlayerRef.UnequipAll()
EndFunction	


 