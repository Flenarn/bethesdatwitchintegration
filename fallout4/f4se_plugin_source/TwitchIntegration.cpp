// F4SE
#include "f4se/PluginAPI.h"
#include "f4se/PapyrusNativeFunctions.h"
#include "f4se_common/f4se_version.h"

// Global
#include <shlobj.h>
#include <iostream>
#include <fstream>
#include <thread>

//Internal
#include "DirectoryChange.h"
#include "Config.h"
#include "RVA.h"
#include "TwitchIntegration.h"

void initScanThread() {
	std::thread TwitchThread(monitorDirectory);
	TwitchThread.detach();
}

bool invalidVersion(const F4SEInterface* f4se) {
	char str[512];
	sprintf_s(str, sizeof(str), "Your game version: v%d.%d.%d.%d is not supported.\n%s will be disabled.",
    GET_EXE_VERSION_MAJOR(f4se->runtimeVersion),
    GET_EXE_VERSION_MINOR(f4se->runtimeVersion),
    GET_EXE_VERSION_BUILD(f4se->runtimeVersion),
    GET_EXE_VERSION_SUB(f4se->runtimeVersion),
    PLUGIN_NAME_LONG);

	MessageBox(NULL, str, PLUGIN_NAME_LONG, MB_OK | MB_ICONEXCLAMATION);
	return false;
}

PluginHandle					g_PluginHandle	= kPluginHandle_Invalid;

F4SEMessagingInterface			*g_Messaging	= NULL;
static F4SEPapyrusInterface*	g_Papyrus		= NULL;

/* Plugin Query */

extern String CommandString;

namespace Injected_Functions {
	void TwitchWriteCommandReturn(StaticFunctionTag* base) {
		//_MESSAGE("pickup the command");
		CHAR my_documents[MAX_PATH];
		HRESULT result = SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, SHGFP_TYPE_CURRENT, my_documents);

		char twitchCommandDump[MAX_PATH];

		sprintf_s(twitchCommandDump, "%s\\My Games\\Fallout4\\F4SE\\TwitchIntegration\\commandFeedback.txt", my_documents);

		const char* path = twitchCommandDump;

		std::ofstream file(path);
		return;
	}

	static BSFixedString TwitchPluginDependency(StaticFunctionTag* Base) {
		BSFixedString result = CommandString.esmToUse.c_str();
		return result;
	}
}

bool Register_Functions(VirtualMachine* vm) {
	vm->RegisterFunction(
		new NativeFunction0 <StaticFunctionTag, void> ("TwitchWriteCommandReturn", "TwitchWriteCommandReturn", Injected_Functions::TwitchWriteCommandReturn, vm));
	vm->RegisterFunction(
		new NativeFunction0 <StaticFunctionTag, BSFixedString>("TwitchPluginDependency", "TwitchPluginDependency", Injected_Functions::TwitchPluginDependency, vm));
	return true;
}

extern "C" {

	void F4SEMessageHandler(F4SEMessagingInterface::Message* msg) {
		if(!msg) {
			return;
		}

		switch(msg->type) {
			case F4SEMessagingInterface::kMessage_GameLoaded:
				initScanThread();
				break;

			default:
				break;
		}
	}

	bool F4SEPlugin_Query(const F4SEInterface* f4se, PluginInfo* info) {
		gLog.OpenRelative(CSIDL_MYDOCUMENTS, "\\My Games\\Fallout4\\F4SE\\f4se_twitch_integration.log");
		gLog.SetPrintLevel(IDebugLog::kLevel_Error);
		gLog.SetLogLevel(IDebugLog::kLevel_DebugMessage);

		_MESSAGE("Twitch Integration v%s", PLUGIN_VERSION_STRING);
		_MESSAGE("Twitch Integration query");

		// Info Structure
		info->infoVersion	= PluginInfo::kInfoVersion;
		info->name			= PLUGIN_NAME_SHORT;	
		info->version		= PLUGIN_VERSION;

		// Store the plugin handle
		g_PluginHandle = f4se->GetPluginHandle();

		// Check if we've been loaded into the Creation Kit
		if (f4se->isEditor) {
			_MESSAGE("F4SE loaded in editor mode, Twitch Intergration plugin disabled.");
			return false;
		}
		
		// Check game version, all version from 1.9.4 and up should function (as long as the runtime version is added to the check), as version independence was achieved thanks to @reg2k's RVA implementation. 
		if(f4se->runtimeVersion == RUNTIME_VERSION_1_10_163 || f4se->runtimeVersion == RUNTIME_VERSION_1_10_162 || f4se->runtimeVersion == RUNTIME_VERSION_1_10_138 || f4se->runtimeVersion == RUNTIME_VERSION_1_10_130 || f4se->runtimeVersion == RUNTIME_VERSION_1_10_120 || f4se->runtimeVersion == RUNTIME_VERSION_1_10_114 || f4se->runtimeVersion == RUNTIME_VERSION_1_10_111 || f4se->runtimeVersion == RUNTIME_VERSION_1_10_106 || f4se->runtimeVersion == RUNTIME_VERSION_1_10_98 || f4se->runtimeVersion == RUNTIME_VERSION_1_10_89 || f4se->runtimeVersion == RUNTIME_VERSION_1_10_82 || f4se->runtimeVersion == RUNTIME_VERSION_1_10_75 || f4se->runtimeVersion == RUNTIME_VERSION_1_10_64 || f4se->runtimeVersion == RUNTIME_VERSION_1_10_50 || f4se->runtimeVersion == RUNTIME_VERSION_1_10_40 || f4se->runtimeVersion == RUNTIME_VERSION_1_10_26 || f4se->runtimeVersion == RUNTIME_VERSION_1_10_20 || f4se->runtimeVersion == RUNTIME_VERSION_1_9_4) { //TODO Add the rest of the supported versions
			_MESSAGE("\nRuntime valid.\n");
		} else {
			invalidVersion(f4se);
		}

		// Get the F4SE messaging interface.
		g_Messaging = (F4SEMessagingInterface *)f4se->QueryInterface(kInterface_Messaging);
		if(!g_Messaging) {
			_MESSAGE("Couldn't get F4SE messaging interface.");
			return false;
		}

		// Version is supported.
		return true;
	}

	bool F4SEPlugin_Load(const F4SEInterface * f4se) {
		_MESSAGE("Twitch Integration plugin load started!");

		if(!g_Messaging->RegisterListener(g_PluginHandle, "F4SE", F4SEMessageHandler)) {
			_MESSAGE("Couldn't register F4SE messaging listener, marking as incompatible");
			return false;
		}

		g_Papyrus = (F4SEPapyrusInterface*)f4se->QueryInterface(kInterface_Papyrus);

		if (g_Papyrus->Register(Register_Functions)) {
			_MESSAGE("Twitch Integration functions registered!");
		}

		RVAManager::UpdateAddresses(f4se->runtimeVersion);

		_MESSAGE("Twitch Integration plugin loaded sucessfully!");
		return true;
	}
};
