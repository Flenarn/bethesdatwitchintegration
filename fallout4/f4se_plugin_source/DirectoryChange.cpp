// F4SE
#include "f4se/PapyrusEvents.h"
#include "f4se/PapyrusUtilities.h"

// Internal
#include "DirectoryChange.h"
#include "RVA.h"
#include "CommandHandler.h"

// Global
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <fstream>
#include <string>
#include <shlobj.h>
#include <cstdint>

//-----------------------------------
// Address for internal console call
//-----------------------------------

bool scanDirectory = true;

void fileManagement() {
	Sleep(100);
	scanDirectory = false;

	CHAR my_documents[MAX_PATH];
	HRESULT result = SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, SHGFP_TYPE_CURRENT, my_documents);

	char twitchCommandDump[MAX_PATH];

	sprintf_s(twitchCommandDump, "%s\\My Games\\Fallout4\\F4SE\\TwitchIntegration\\", my_documents);

	//_MESSAGE("Temporarily stopped file scanning.");
	SetCurrentDirectory(twitchCommandDump);
	std::string command;
	std::ifstream commandFile;

	commandFile.open("command.txt", std::ifstream::in);

	if(commandFile.is_open()) {
		while (commandFile.good()){
			getline(commandFile, command);
			commandHandler(command.c_str());
		}
		commandFile.close();
	} else {
		//scanDirectory = true;
		//monitorDirectory();
		_MESSAGE("ERROR: Unable to open file.");
	}

	if(remove("command.txt") != 0) {
		_MESSAGE("ERROR: Error deleting file.");
	} else {
		_MESSAGE("Deleted file.");
	}
	scanDirectory = true;
	monitorDirectory();
}

void monitorDirectory() {
	CHAR my_documents[MAX_PATH];
	HRESULT result = SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, SHGFP_TYPE_CURRENT, my_documents);

	char twitchCommandDump[MAX_PATH];

	sprintf_s(twitchCommandDump, "%s\\My Games\\Fallout4\\F4SE\\TwitchIntegration\\", my_documents);

	LPTSTR lpDir = twitchCommandDump;

	//_MESSAGE("\nStarted monitoring file directory.\n");

	DWORD dwWaitStatus;
	HANDLE dwChangeHandles[2];
	TCHAR lpDrive[4];
	TCHAR lpFile[_MAX_FNAME];
	TCHAR lpExt[_MAX_EXT];

	_tsplitpath_s(lpDir, lpDrive, 4, NULL, 0, lpFile, _MAX_FNAME, lpExt, _MAX_EXT);

	lpDrive[2] = (TCHAR)'\\';
	lpDrive[3] = (TCHAR)'\0';

	// Watch the directory for file creation and deletion.

	dwChangeHandles[0] = FindFirstChangeNotification(
		lpDir,							// Directory to watch
		FALSE,							// Don't watch subtree.
		FILE_NOTIFY_CHANGE_FILE_NAME);	// Watch file name changes.

	if (dwChangeHandles[0] == INVALID_HANDLE_VALUE) {
		_MESSAGE("\n ERROR: FindFirstChangeNotification failed.\n");
	}

	// Watch the subtree for directory creation and deletion.

	dwChangeHandles[1] = FindFirstChangeNotification(
		lpDrive,						// Directory to watch.
		TRUE,							// Watch the subtree.
		FILE_NOTIFY_CHANGE_DIR_NAME);	// Watch directory name changes.

	if (dwChangeHandles[1] == INVALID_HANDLE_VALUE) {
		_MESSAGE("\n ERROR: FindFirstChangeNotification failed.\n");
	}

	// Final validation check on our handles.

	if ((dwChangeHandles[0] == NULL) || (dwChangeHandles[1] == NULL)) {
		_MESSAGE("\n ERROR: Unexpected NULL from FindFirstChangeNotification.\n");
	}

	while (scanDirectory) {
		// Wait for notification.

		dwWaitStatus = WaitForMultipleObjects(2, dwChangeHandles, FALSE, INFINITE);

		switch (dwWaitStatus) {

			case WAIT_OBJECT_0:
				// A file was created, renamed or deleted in the directory.
				//_MESSAGE("\nFile change detected!\n");
				fileManagement();
				return;
				break;


			case WAIT_OBJECT_0 + 1:
				// A directory was created, renamed or deleted.
				//_MESSAGE("\nDirectory change detected!\n");
				break;

			case WAIT_TIMEOUT:
				// Timeout occured.
				//_MESSAGE("\nNo changes in the timeout period.\n");
				break;

			default:
				_MESSAGE("\n ERROR: Unhandled dwWaitStatus.\n");
				break;
		}
	}
}
