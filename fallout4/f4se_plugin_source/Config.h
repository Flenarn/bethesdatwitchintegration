#pragma once

// F4SE
#include "f4se_common\f4se_version.h"

//-----------------------
// Plugin Information
//-----------------------
#define PLUGIN_VERSION				1
#define PLUGIN_VERSION_STRING		"1.0"
#define PLUGIN_NAME_SHORT			"F4TWITCH"
#define PLUGIN_NAME_LONG			"Fallout 4 Twitch Integration"
#define MINIMUM_RUNTIME_VERSION     RUNTIME_VERSION_1_9_4