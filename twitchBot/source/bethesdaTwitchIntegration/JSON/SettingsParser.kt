package bethesdaTwitchIntegration.json

import bethesdaTwitchIntegration.json.CommandValidation.defaultPlugin

import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser

import java.io.FileReader
import java.util.Locale

object SettingsParser {

    // Util. Properties
    var tiltifyMode = false
    var campaignID = "null"
    var systemEnabled = false
    const val sharedPrefix = "#"
    var channelToJoin = "null"
    var subscriptionPoints: Long = 0
    var pointsPerMinute: Long = 2
    var pointsWhenDisabled = true
    var bitMultiplier: Long = 0
    var customDocumentsPath = "null"
    var randomspawncost: Long = 0

    // Command Properties
    var lastDefaultCommand: Long = 0
    var lastSpecialCommand: Long = 0
    var commandDelayMillis: Long = 0
    var lastCommandGlobal: Long = 0
    var specialCommandDelayMillis: Long = 0
    var gameJSON = "null"
    var gamePath = "null"

    fun settingsParser() {
        val jsonParser = JSONParser()

        try {
            val jsonObject = jsonParser.parse(FileReader("data/settings.json")) as JSONObject

            tiltifyMode = jsonObject["tiltifyMode"] as Boolean
            campaignID = jsonObject["campaignID"] as String

            channelToJoin = (jsonObject["channelToConnectTo"] as String).lowercase(Locale.getDefault())

            commandDelayMillis = jsonObject["commandDelayMillis"] as Long
            specialCommandDelayMillis = jsonObject["specialCommandDelayMillis"] as Long

            pointsPerMinute = jsonObject["pointsPerMinute"] as Long
            pointsWhenDisabled = jsonObject["pointsWhenDisabled"] as Boolean

            subscriptionPoints = jsonObject["subscriptionPoints"] as Long

            bitMultiplier = jsonObject["bitMultiplier"] as Long

            customDocumentsPath = jsonObject["customDocumentsPath"] as String

            randomspawncost = jsonObject["randomSpawnCost"] as Long

            when(jsonObject["gamemode"] as String) {
                "fallout4" -> {
                    gameJSON = "Fallout4"
                    defaultPlugin = "Fallout4.esm"
                    gamePath = "Fallout4/F4SE"
                }

                "skyrimle" -> {
                    gameJSON = "SkyrimLE"
                    defaultPlugin = "Skyrim.esm"
                    gamePath = "Skyrim/SKSE"
                }

                "skyrimse" -> {
                    gameJSON = "SkyrimSE"
                    defaultPlugin = "Skyrim.esm"
                    gamePath = "Skyrim Special Edition/SKSE"
                }

                "falloutnewvegas" -> {
                    gameJSON = "NewVegas"
                    defaultPlugin = "FalloutNV.esm"
                    gamePath = "FalloutNV/NVSE"
                }

                "fallout3" -> {
                    gameJSON = "Fallout3"
                    defaultPlugin = "Fallout3.esm"
                    gamePath = "Fallout3/FOSE"
                }

                "oblivion" -> {
                    gameJSON = "Oblivion"
                    defaultPlugin = "Oblivion.esm"
                    gamePath = "Oblivion/OBSE"
                }
            }

        } catch (exception : Exception) {
            exception.printStackTrace()
        }
    }

}