package bethesdaTwitchIntegration.json

import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import java.io.FileReader
import java.io.FileWriter
import java.util.Locale

object PointLogic {

    private fun parseUsers() : JSONObject? {
        val jsonParser = JSONParser()
        try {
            return jsonParser.parse(FileReader("data/json_viewers.json")) as JSONObject
        } catch (exception : Exception) {
            exception.printStackTrace()
        }
        return null
    }

    private fun writeViewers(jsonObject : JSONObject) {
        try {
            val fileWriter = FileWriter("data/json_viewers.json")
            fileWriter.write(jsonObject.toJSONString())
            fileWriter.close()
        } catch (exception : Exception) {
            exception.printStackTrace()
        }
    }

    fun purgeUsers(value : Int) {
        val jsonObject = parseUsers()

        for (`object` in jsonObject!!.keys) {
            val currentEntry = `object` as String
            val currentEntryPoints = jsonObject[currentEntry] as Long
            if(currentEntryPoints < value) {
                removeUser(currentEntry)
            }
        }
    }

    fun userExists(username : String) : Boolean {
        return parseUsers()!!.containsKey(username.lowercase(Locale.getDefault()))
    }

    fun addUser(username : String) {
        val jsonObject = parseUsers()

        if(!userExists(username)) {
            jsonObject!![username.lowercase(Locale.getDefault())] = 0
            writeViewers(jsonObject)
        }
    }

    private fun removeUser(username : String) {
        val jsonObject = parseUsers()
        for (keyIterator in jsonObject!!.keys as Iterable<String?>) {
            if (keyIterator.equals(username, ignoreCase = true)) {
                jsonObject.remove(username)
                writeViewers(jsonObject)
            }
        }
    }

    fun modifyUserPoints(pointsToModify : Int, username: String) {
        if (userExists(username)) {
            val jsonObject = parseUsers()
            jsonObject!!.replace(username, (jsonObject[username] as Long).toInt() + pointsToModify)
            writeViewers(jsonObject)
        } else {
            addUser(username)
        }
    }

    fun userPoints(username : String) : Int {
        return if(userExists(username)) {
            val jsonObject = parseUsers()
            (jsonObject!![username] as Long).toInt()
        } else {
            addUser(username)
            0
        }
    }
}