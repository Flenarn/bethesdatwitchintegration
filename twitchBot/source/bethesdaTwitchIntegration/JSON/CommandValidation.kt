package bethesdaTwitchIntegration.json

import bethesdaTwitchIntegration.json.SettingsParser.gameJSON
import org.bethesdaTwitchIntegration.BethesdaTwitchIntegration

import org.json.simple.JSONArray
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser

import java.io.FileReader

object CommandValidation {

    // Item Command Properties
    var itemCost = 0
    var itemID = "null"
    var itemsMultiplied = false

    // Spawn Command Properties
    var npcCost = 0
    var npcToSpawn = "null"
    var npcAmountToSpawn = 0
    var allowedInInterior = true

    // Weather Command Properties
    var weatherID = "null"
    var weatherCost = 0

    // Misc Command Properties
    var miscCost = 0
    var miscCommand = "null"

    // Misc Properties
    var defaultPlugin = "null"
    var pluginToLookFor = "null"

    fun validateSpawn(spawnCommand : String?): Boolean {
        val jsonParser = JSONParser()
        try {
            val jsonObject =
                jsonParser.parse(FileReader("data/$gameJSON/${gameJSON}spawns.json")) as JSONObject
            for (keyIterator in jsonObject.keys as Iterable<String?>) {
                if (keyIterator.equals(spawnCommand, ignoreCase = true)) {
                    val jsonArray = jsonObject[keyIterator] as JSONArray
                    npcCost = (jsonArray[0] as Long).toInt()
                    npcToSpawn = (jsonArray[1] as String?)!!
                    npcAmountToSpawn = (jsonArray[2] as Long).toInt()
                    allowedInInterior = jsonArray[3] as Boolean
                    pluginToLookFor = if (jsonArray.size == 5) {
                        (jsonArray[4] as String?)!!
                    } else defaultPlugin
                    return true
                }
            }
            return false
        } catch (exception : Exception) {
            exception.printStackTrace()
        }
        return false
    }

    fun validateMisc(miscCommand : String?): Boolean {
        val jsonParser = JSONParser()
        try {
            val jsonObject =
                jsonParser.parse(FileReader("data/$gameJSON/${gameJSON}misc.json")) as JSONObject
            for (keyIterator in jsonObject.keys as Iterable<String?>) {
                if (keyIterator.equals(miscCommand, ignoreCase = true)) {
                    val jsonArray = jsonObject[keyIterator] as JSONArray
                    miscCost = (jsonObject[0] as Long).toInt()
                    CommandValidation.miscCommand = (jsonArray[1] as String?)!!
                    return true
                }
            }
            return false
        } catch (exception : Exception) {
            exception.printStackTrace()
        }
        return false
    }

    fun validateWeather(weatherCommand : String?): Boolean {
        val jsonParser = JSONParser()
        try {
            val jsonObject = jsonParser.parse(FileReader("data/$gameJSON/${gameJSON}weathers.json")) as JSONObject
            BethesdaTwitchIntegration.sendMessage(weatherCommand)
            for (keyIterator in jsonObject.keys as Iterable<String?>) {
                if (keyIterator.equals(weatherCommand, ignoreCase = true)) {
                    val jsonArray = jsonObject[keyIterator] as JSONArray
                    weatherCost = (jsonArray[0] as Long).toInt()
                    weatherID = (jsonArray[1] as String?)!!
                    pluginToLookFor = if (jsonArray.size == 3) {
                        (jsonArray[2] as String?)!!
                    } else defaultPlugin
                    return true
                }
            }
            return false
        } catch (exception : Exception) {
            exception.printStackTrace()
        }
        return false
    }

    fun validateGive(command : String) : Boolean {
        val jsonParser = JSONParser()

        try {
            val jsonObject = jsonParser.parse(FileReader("data/$gameJSON/${gameJSON}items.json")) as JSONObject
            for (keyIterator in jsonObject.keys as Iterable<String>) {
                if (keyIterator.equals(command, ignoreCase = true)) {
                    val jsonArray = jsonObject[keyIterator] as JSONArray
                    itemCost = (jsonArray[0] as Long).toInt()
                    itemsMultiplied = command.startsWith("ammo_") || command.startsWith("resource_")
                    itemID = (jsonArray[1] as String?)!!
                    pluginToLookFor = if (jsonArray.size == 3) {
                        (jsonArray[2] as String?)!!
                    } else defaultPlugin
                    return true
                }
            }
            return false
        } catch (exception : Exception) {
            exception.printStackTrace()
        }
        return false
    }

}