package bethesdaTwitchIntegration.misc

import bethesdaTwitchIntegration.commands.SpawnCommand
import bethesdaTwitchIntegration.json.PointLogic
import bethesdaTwitchIntegration.json.SettingsParser
import bethesdaTwitchIntegration.json.SettingsParser.customDocumentsPath
import bethesdaTwitchIntegration.json.SettingsParser.gamePath
import bethesdaTwitchIntegration.tiltify.TiltifyVariant

import java.io.File
import java.nio.file.FileSystems
import java.nio.file.Paths
import java.nio.file.StandardWatchEventKinds
import java.nio.file.Path

class DirectoryMonitoring : Thread() {
    override fun run() {
        val documentsPath: String = if (customDocumentsPath.equals("default", ignoreCase = true)) {
            System.getProperty("user.home") + File.separator + "Documents"
        } else {
            customDocumentsPath
        }
        try {
            FileSystems.getDefault().newWatchService().use { watchService ->
                val path = Paths.get("$documentsPath/My Games/$gamePath/TwitchIntegration")
                path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE)
                while (true) {
                    val watchKey = watchService.take()
                    for (watchEvent in watchKey.pollEvents()) {
                        val context = watchEvent.context() as Path
                        if (context.toString().equals("commandFeedback.txt", ignoreCase = true)) {
                            if (SettingsParser.tiltifyMode) {
                                val file = File("$documentsPath/My Games/$gamePath/TwitchIntegration/$context")
                                if (file.delete()) {
                                    //Do nothing, we don't care for point reduction in this instance.
                                }
                            } else {
                                val file = File("$documentsPath/My Games/$gamePath/TwitchIntegration/$context")
                                if (file.delete()) {
                                    val commandReturn: List<String> = SpawnCommand.lastCommandFired.split(" ")
                                    PointLogic.modifyUserPoints(commandReturn[1].toInt(), commandReturn[0])
                                }
                            }
                        }
                    }
                    val valid = watchKey.reset()
                    if (!valid) {
                        break
                    }
                }
            }
        } catch (exception : Exception) {
            exception.printStackTrace()
        }
    }
}