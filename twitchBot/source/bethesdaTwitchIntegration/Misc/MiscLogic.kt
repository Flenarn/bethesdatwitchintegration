package bethesdaTwitchIntegration.misc

import bethesdaTwitchIntegration.json.SettingsParser.commandDelayMillis
import bethesdaTwitchIntegration.json.SettingsParser.customDocumentsPath
import bethesdaTwitchIntegration.json.SettingsParser.gamePath
import bethesdaTwitchIntegration.json.SettingsParser.lastCommandGlobal
import bethesdaTwitchIntegration.json.SettingsParser.lastDefaultCommand
import bethesdaTwitchIntegration.json.SettingsParser.lastSpecialCommand
import bethesdaTwitchIntegration.json.SettingsParser.specialCommandDelayMillis

import java.io.File
import java.io.FileNotFoundException
import java.io.PrintWriter
import java.nio.file.Files
import java.nio.file.Paths

object MiscLogic {

    /*
     * Check if string value is a valid integer value.
     */

    fun isInteger(stringToCheck: String?): Boolean {
        if (stringToCheck == null) {
            return false
        }
        val length: Int = stringToCheck.length
        if (length == 0) {
            return false
        }

        var i = 0
        if (stringToCheck[0] == '.') {
            if (length == 1) {
                return false
            }
            i = 1
        }
        while (i < length) {
            val c: Char = stringToCheck[i]
            if (c < '0' || c > '9') {
                return false
            }
            i++
        }
        return true
    }

    /*
     * Creates a command for xSE to detect and execute.
     */
    fun createCommandCall(command: String?) {
        val documentsPath: String = if (customDocumentsPath.equals("default", ignoreCase = true)) {
            System.getProperty("user.home") + File.separator + "Documents"
        } else {
            customDocumentsPath
        }

        try {
            PrintWriter("$documentsPath/My Games/$gamePath/TwitchIntegration/command.txt").use { out ->
                out.println(
                    command
                )
            }
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
    }

    /*
     * Clears directories in use on boot to make sure no left-over files mess up anything.
     */

    fun startCleanup() {
        lastDefaultCommand = System.currentTimeMillis() - commandDelayMillis
        lastSpecialCommand = System.currentTimeMillis() - specialCommandDelayMillis
        lastCommandGlobal = System.currentTimeMillis() - 250
        val documentsPath: String = if (customDocumentsPath.equals("default", ignoreCase = true)) {
            System.getProperty("user.home") + File.separator + "Documents"
        } else {
            customDocumentsPath
        }

        //Make sure the path we call use down below actually exists, if not, create it.
        if (!Files.exists(Paths.get("$documentsPath/My Games/$gamePath/TwitchIntegration/"))) {
            Files.createDirectories(Paths.get("$documentsPath/My Games/$gamePath/TwitchIntegration/"))
        }

        try {
            val command = File("$documentsPath/My Games/$gamePath/TwitchIntegration/command.txt")
            val commandFeedback = File("$documentsPath/My Games/$gamePath/TwitchIntegration/commandFeedback.txt")
            command.delete()
            commandFeedback.delete()
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }
}