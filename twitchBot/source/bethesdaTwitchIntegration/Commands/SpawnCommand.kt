package bethesdaTwitchIntegration.commands

import bethesdaTwitchIntegration.json.CommandValidation
import bethesdaTwitchIntegration.json.CommandValidation.allowedInInterior
import bethesdaTwitchIntegration.json.CommandValidation.npcAmountToSpawn
import bethesdaTwitchIntegration.json.CommandValidation.npcCost
import bethesdaTwitchIntegration.json.CommandValidation.npcToSpawn
import bethesdaTwitchIntegration.json.CommandValidation.pluginToLookFor
import bethesdaTwitchIntegration.json.PointLogic
import bethesdaTwitchIntegration.json.SettingsParser.lastCommandGlobal
import bethesdaTwitchIntegration.json.SettingsParser.lastDefaultCommand
import bethesdaTwitchIntegration.misc.MiscLogic
import org.bethesdaTwitchIntegration.BethesdaTwitchIntegration

object SpawnCommand {

    var lastCommandFired = "null 0"

    fun handleSpawn(username : String, spawnCommand: String?) {
        if (CommandValidation.validateSpawn(spawnCommand)) {
            val allowedInInteriorInt = if (allowedInInterior) 1 else  0
            if (PointLogic.userPoints(username) < npcCost) {
                BethesdaTwitchIntegration.sendMissingPoints(username, npcCost)
            } else {
                PointLogic.modifyUserPoints(-npcCost, username)
                val commandForxSE = "spawn $npcToSpawn $npcAmountToSpawn $allowedInInteriorInt $pluginToLookFor"
                MiscLogic.createCommandCall(commandForxSE)
                lastDefaultCommand = System.currentTimeMillis()
                lastCommandGlobal = System.currentTimeMillis()
                lastCommandFired = "$username $npcCost"
            }
        } else {
            BethesdaTwitchIntegration.sendInvalidCommand(username)
        }
    }
}