package bethesdaTwitchIntegration.commands

import bethesdaTwitchIntegration.json.CommandValidation
import bethesdaTwitchIntegration.json.CommandValidation.itemCost
import bethesdaTwitchIntegration.json.CommandValidation.itemID
import bethesdaTwitchIntegration.json.CommandValidation.itemsMultiplied
import bethesdaTwitchIntegration.json.CommandValidation.pluginToLookFor
import bethesdaTwitchIntegration.json.PointLogic
import bethesdaTwitchIntegration.json.SettingsParser.lastCommandGlobal
import bethesdaTwitchIntegration.json.SettingsParser.lastDefaultCommand
import bethesdaTwitchIntegration.misc.MiscLogic
import org.bethesdaTwitchIntegration.BethesdaTwitchIntegration

object GiveCommand {

    fun handleGive(username: String, giveCommand: String, amount: Int) {
        if (CommandValidation.validateGive(giveCommand)) {
            if (PointLogic.userPoints(username) < itemCost) {
                BethesdaTwitchIntegration.sendMissingPoints(username, itemCost)
            } else {
                PointLogic.modifyUserPoints(-itemCost, username)
                val itemAmount: Int = if (itemsMultiplied) {
                    amount * 10
                } else {
                    amount
                }
                val commandForxSE = "player.additem $itemID $itemAmount $pluginToLookFor"
                MiscLogic.createCommandCall(commandForxSE)
                lastDefaultCommand = System.currentTimeMillis()
                lastCommandGlobal = System.currentTimeMillis()
            }
        } else {
            BethesdaTwitchIntegration.sendInvalidCommand(username)
        }
    }
}