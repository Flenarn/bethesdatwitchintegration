package bethesdaTwitchIntegration.tiltify

import bethesdaTwitchIntegration.json.SettingsParser

import org.json.simple.JSONArray
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser

import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.StringBuilder
import java.util.Timer
import java.util.TimerTask

import org.apache.http.HttpEntity
import org.apache.http.HttpHeaders
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.config.CookieSpecs
import org.apache.http.client.config.RequestConfig
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.utils.URIBuilder
import org.apache.http.impl.client.HttpClients

object TiltifyVariant {

    private const val tiltifyCredential = "Bearer "

    //How often we want the timer to run, by default every 2 seconds.
    private const val timerSeconds = 2

    //Specific tag to track, currently checks the name of the donor for it, tag will later be specified through settings file.
    var tagToTrack = "#WeRateDogs"

    //Grabs the first donation ID (Latest ID by descending order) and boots from there.
    var firstDonation = 0

    var lastDonationChecked = 0

    var initialBoot = true
    var initialIDChecked = false

    private const val maxDonationListed = 100

    fun tiltifyLoop() {
        val httpClient: HttpClient = HttpClients.custom()
            .setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build()).build()

        val mainTimer = Timer()
        mainTimer.schedule(object : TimerTask() {
            override fun run() {
                try {
                    if (initialBoot) {
                        initialBoot = false
                        val uriBuilder =
                            URIBuilder("https://tiltify.com/api/v3/campaigns/${SettingsParser.campaignID}/donations?count=100")
                        val httpGet = HttpGet(uriBuilder.build())
                        httpGet.setHeader(
                            HttpHeaders.AUTHORIZATION,
                            tiltifyCredential
                        )
                        httpGet.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                        val httpResponse: HttpResponse = httpClient.execute(httpGet)
                        val httpEntity: HttpEntity? = httpResponse.entity
                        if (httpEntity != null) {
                            val reader = BufferedReader(InputStreamReader(httpEntity.content))
                            var output: String?
                            val response = StringBuilder()

                            while (reader.readLine().also { output = it } != null) {
                                response.append(output)
                            }

                            reader.close()
                            val jsonParser = JSONParser()
                            val responseObject = jsonParser.parse(response.toString()) as JSONObject
                            val dataArray = responseObject["data"] as JSONArray
                            val donation = dataArray[dataArray.size - 1] as JSONObject
                            firstDonation = donation["id"].toString().toInt()
                        }
                        //First donation needs to be made by us, it will be ignored by system. (Assuming we are doing a fresh campaign for this event alone, otherwise we'll need a boot ID.).
                        lastDonationChecked = firstDonation
                        println("Initialization done!")
                        println("First donation ID: $firstDonation")
                    }

                    // We prep the request string here, based on if it's the first time we're doing the donation check or not.
                    val requestString: String
                    if (initialIDChecked) {
                        requestString =
                            "https://tiltify.com/api/v3/campaigns/${SettingsParser.campaignID}/donations?count=2&after=$firstDonation"
                        initialIDChecked = true
                    } else {
                        requestString =
                            "https://tiltify.com/api/v3/campaigns/${SettingsParser.campaignID}/donations?count=2&after=$lastDonationChecked"
                    }

                    val uriBuilder = URIBuilder(requestString)
                    val httpGet = HttpGet(uriBuilder.build())
                    httpGet.setHeader(
                        HttpHeaders.AUTHORIZATION,
                        tiltifyCredential
                    )
                    httpGet.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    val httpResponse: HttpResponse = httpClient.execute(httpGet)
                    val httpEntity: HttpEntity? = httpResponse.entity

                    if (httpEntity != null) {
                        val reader = BufferedReader(InputStreamReader(httpEntity.content))
                        var output: String?
                        val response = StringBuilder()
                        while (reader.readLine().also { output = it } != null) {
                            response.append(output)
                        }
                        reader.close()
                        val jsonParser = JSONParser()
                        val responseObject = jsonParser.parse(response.toString()) as JSONObject
                        val dataArray = responseObject["data"] as JSONArray
                        if (!dataArray.isEmpty()) {
                            println("=======================================================")
                            for (objectBase in dataArray) {
                                val donation = objectBase as JSONObject

                                println("ID in array order: " + donation["id"] + ", amount: $" + donation["amount"])
                                lastDonationChecked = donation["id"].toString().toInt()

                                val donationAmount = donation["amount"].toString().substring(0, donation["amount"].toString().indexOf(".")).toInt()

                                if (donation["name"].toString().contains(tagToTrack) && donationAmount >= 25) {
                                    DonationManagement.handleDonationDoggo()
                                } else if(donationAmount <= maxDonationListed || (donation["name"].toString().contains(tagToTrack) && donationAmount < 25)) {
                                    DonationManagement.handleDonation(donationAmount)
                                } else {
                                    DonationManagement.handleDonation(maxDonationListed)
                                }
                            }
                        }
                    } else {
                        println("CRITICAL FAILURE - TILTIFY API 403/404 RESPONSE")
                    }
                } catch (ignored: Exception) {
                    //Exceptions are ignored, just catch to not terminate the process.
                }
            }
        }, 0, (timerSeconds * 1000).toLong())
    }

}