package bethesdaTwitchIntegration.tiltify

import bethesdaTwitchIntegration.json.CommandValidation
import bethesdaTwitchIntegration.json.SettingsParser
import bethesdaTwitchIntegration.misc.MiscLogic

import org.json.simple.JSONArray
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser

import java.io.FileReader

object DonationManagement {

    fun handleDonation(donationAmount : Int) {
        val donationCommand = getDonationListingEntry(donationAmount)
        if (donationCommand != "null") {
            MiscLogic.createCommandCall(donationCommand)
            Thread.sleep(650)
        }
    }

    /**
     * A custom Barbas gets spawned here for the #WeRateDogs alongside any donation with a value of $25 or greater.
     */
    fun handleDonationDoggo() {
        MiscLogic.createCommandCall("spawn CustomBarbasGoesHere 1 ${CommandValidation.defaultPlugin}")
    }

    private fun getDonationListingEntry(donationAmount : Int): String {
        val jsonParser = JSONParser()

        try {
            val jsonObject = jsonParser.parse(FileReader("data/${SettingsParser.gameJSON}/${SettingsParser.gameJSON}tiltify.json")) as JSONObject
            for (keyIterator in jsonObject.keys as Iterable<String?>) {
                val strippedString = keyIterator!!.substring(0, keyIterator.indexOf("_"))
                if (strippedString.equals(donationAmount.toString(), ignoreCase = true)) {
                    val jsonArray = jsonObject[keyIterator] as JSONArray
                    val type = jsonArray[0] as String?

                    when {
                        type.equals("ITEM", ignoreCase = true) -> {
                            val id = jsonArray[1] as String?
                            val amount = (jsonArray[2] as Long).toInt()
                            val pluginToLookFor = if (jsonArray.size == 4) {
                                (jsonArray[3] as String?)!!
                            } else CommandValidation.defaultPlugin
                            return "player.additem $id $amount $pluginToLookFor"
                        }
                        type.equals("NPC", ignoreCase = true) -> {
                            val id = jsonArray[1] as String?
                            val amount = (jsonArray[2] as Long).toInt()
                            val pluginToLookFor = if (jsonArray.size == 4) {
                                (jsonArray[3] as String?)!!
                            } else CommandValidation.defaultPlugin
                            return "spawn $id $amount 1 $pluginToLookFor"
                        }
                        type.equals("COMMAND", ignoreCase = true) -> {
                            return jsonArray[1] as String
                        }
                    }
                }
            }
        } catch (exception : Exception) {
            exception.printStackTrace()
        }
        return "null"
    }
}